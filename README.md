<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About the project

Check out the [Project definition](https://david-valbuena.notion.site/Reto-af876a2875a3408c8095ab62408030fa).

![Laravel](https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white)
![VueJs](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vue.js&logoColor=4FC08D)
![MariaDb](https://img.shields.io/badge/MariaDB-003545?style=for-the-badge&logo=mariadb&logoColor=white)

> **Requires:**
- **[PHP 8.1+](https://php.net/releases/)**
- **[Laravel 10+](https://github.com/laravel/laravel)**
- **[MySQL 8.0+](https://mariadb.org/)**

## Installation

- composer install
- npm install
- cp .env.example .env
- update the .env file with the database credentials and mailing information
- php artisan key:generate
- php artisan migrate --seed
- php artisan storage:link
- php artisan app:create-admin  "email@domain.ext" and "8CharsPass"
- npm run build
- add to the server crontab: ```* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1```

## Development

- php artisan serve _or create a local vhost in apache or nginx_
- npm run dev

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
