<?php

namespace App\Console\Commands;

use App\Support\Definitions\Rol;
use App\Support\Definitions\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un usuario administrador';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $result = false;
        try {
            $result = DB::table('users')->insert([
                'name' => fake()->name(),
                'lastname' => fake()->lastName(),
                'phone' => fake()->lastName(),
                'email' => $this->ask('Correo electronico?'),
                'password' => Hash::make($this->secret('Clave?')),
                'status_id' => Status::ACTIVE->value,
                'rol_id' => Rol::ADMINISTRATOR->value,
                'email_verified_at' => now(),
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if ($result) {
            $this->info('Usuario creado!');
        } else {
            $this->error('Error al crear usuario');
        }
    }
}
