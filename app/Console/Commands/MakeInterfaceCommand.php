<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeInterfaceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:interface {name : El nombre de la interfaz}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un nuevo archivo de interfaz.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->argument('name');

        $filename = "app/Support/Interfaces/{$name}.php";

        if (file_exists($filename)) {
            $this->error('El archivo de interfaz ya existe!');
            return;
        }

        $interface = "<?php\n\nnamespace App\Support\Interfaces;\n\ninterface {$name}\n{\n
            // Define tus métodos aquí\n}\n";

        file_put_contents($filename, $interface);

        $this->info("La interfaz {$name} se ha creado con éxito.");
    }
}
