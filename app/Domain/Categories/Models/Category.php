<?php

namespace App\Domain\Categories\Models;

use App\Support\Definitions\Status;
use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $description
 * @property \App\Support\Definitions\Status|null $status
 */
class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'status_id',
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn(int $value) => match ($value) {
                Status::ACTIVE->value => Status::ACTIVE,
                Status::INACTIVE->value => Status::INACTIVE,
                default => null
            },
            set: static function ($value) {
                return $value ? Status::ACTIVE->value : Status::INACTIVE->value;
            }
        );
    }

    protected static function newFactory(): Factory
    {
        return CategoryFactory::new();
    }
}