<?php

namespace App\Domain\Orders\Actions;

use App\Domain\Orders\Models\Order;
use App\Domain\Products\Models\Product;
use App\Support\Interfaces\IAction;
use App\Support\Definitions\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class StoreOrder implements IAction
{
    /**
     * @param array $params
     * @return bool|int
     */
    public static function execute(array $params): bool|int
    {
        $ids = array_column($params, 'id');

        $products = Product::select('id', 'stock', 'price', 'status_id')->whereIn('id', $ids)->get();

        $stockErrors = self::validateStock($products, $params);

        if ($stockErrors) {
            return false;
        }

        $total = 0;
        foreach ($products as $product) {
            $total += $product->price * $params[$product->id]['amount'];
        }
        $order = new Order();
        $order->date = date('Y-m-d');
        $order->value = $total;
        $order->bill = Str::random(3) . time();
        //$order->user_id = Auth::id();  //TODO: revisar la clase para que llegue
        $order->user_id = session()->get('authData');  //TODO: revisar la clase para que llegue
        $order->status_id = Status::ACTIVE->value;

        if ($order->save()) {
            self::syncProducts($order, $products, $params);
            return $order->id;
        }

        return false;
    }

    private static function syncProducts(Order $order, Collection  $products, array $params): void
    {
        $productsData = [];
        foreach ($products as $product) {
            $quantity = $params[$product->id]['amount'];

            $productsData[$product->id] = [
                'order_id' => $order->id,
                'product_id' => $product->id,
                'price' => $product->price,
                'quantity' => $quantity,
                'total_price' => $quantity * $product->price
            ];

            $product->stock -= $quantity;
            $product->save();
        }
        $order->products()->sync($productsData);
    }

    private static function validateStock(Collection $products, array $params): bool
    {
        $error = false;

        foreach ($products as $product) {
            if ($params[$product->id]['amount'] > $product->stock) {
                $error = true;
                break;
            }
        }

        return $error;
    }
}
