<?php

namespace App\Domain\Orders\Actions;

use App\Domain\Orders\Models\Order;
use App\Support\Interfaces\IAction;

class UpdateOrderStatus implements IAction
{
    /**
     * @param array $params
     * @return bool|int
     */
    public static function execute(array $params): bool|int
    {
        return Order::where('id', $params['id'])->update(['status_id' => $params['status_id']]);
    }
}
