<?php

namespace App\Domain\Orders\Actions;

use App\Domain\Orders\Models\Order;
use App\Support\Interfaces\IAction;
use App\Support\Definitions\Status;

class ValidateOrderStatus implements IAction
{
    /**
     * @param array $params
     * @return bool|int
     */
    public static function execute(array $params): bool|int
    {
        $id = $params['order_id'];
        $order = Order::find($id);
        return $order->status_id === Status::ACTIVE->value;
    }
}