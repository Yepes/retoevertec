<?php

namespace App\Domain\Orders\Models;

use App\Support\Definitions\Status;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Domain\Products\Models\Product;
use App\Domain\Users\Models\User;
use Database\Factories\OrderFactory;

/**
 * @property string $id
 * @property float $date
 * @property float $value
 * @property string $bill
 * @property Status|null $status_id
 * @property int $user_id
 */
class Order extends Model
{
    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'orders_products', 'order_id', 'product_id')
            ->select('products.id', 'description', 'image', 'slug')
            ->withPivot(['price', 'quantity', 'total_price']);
    }

    protected static function newFactory(): Factory
    {
        return OrderFactory::new();
    }
}