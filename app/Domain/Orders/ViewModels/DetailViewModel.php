<?php

namespace App\Domain\Orders\ViewModels;

use App\Domain\Payments\Models\Payment;
use App\Domain\Orders\Models\Order;
use App\Support\Definitions\Status;
use App\Support\ViewModels\ViewModel;

class DetailViewModel extends ViewModel
{
    public function toArray(): array
    {
        $newPayment = false;
        $currentPaymentUrl = false;

        $model = $this->model;

        if ($model->status_id === Status::ACTIVE->value) {
            $payment = Payment::select('id', 'status_id', 'process_url')
                ->where('order_id', $model->id)
                ->orderBy('id', 'desc')->first();

            if ($payment) {
                $newPayment = match ($payment->status_id) {
                    Status::REJECTED->value, Status::FAILED->value => true,
                    default => false
                };

                if ($payment->status_id === Status::ACTIVE->value) {
                    $currentPaymentUrl = $payment->process_url;
                }
            } else {
                $newPayment = true;
            }
        }

        return [
            'order' => $model,
            'products' => $model->products,
            'status' => Status::toArray(),
            'newPayment' => $newPayment,
            'currentPaymentUrl' => $currentPaymentUrl
        ];
    }
}
