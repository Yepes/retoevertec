<?php

namespace App\Domain\Payments\Actions;

use App\Domain\Payments\Models\Payment;
use App\Support\Definitions\Status;
use App\Support\Interfaces\IAction;
use Illuminate\Database\Eloquent\Model;

class GetOrderPayments implements IAction
{
    /**
     * @param array $params
     * @return bool|int|Model
     */
    public static function execute(array $params): bool|int|Model
    {
        $orderId = $params['order_id'];

        $payment = Payment::select('id', 'request_id', 'payment_type')
            ->where('order_id', $orderId)
            ->whereIn('status_id', [
                Status::ACTIVE->value,
                Status::PENDING->value,
                Status::APPROVED_PARTIAL->value,
                Status::PARTIAL_EXPIRED->value,
                Status::REJECTED->value,
                Status::FAILED->value
            ])
            ->orderBy('id', 'desc')
            ->first();

        return $payment ?: false;
    }
}
