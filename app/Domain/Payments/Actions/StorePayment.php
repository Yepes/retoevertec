<?php

namespace App\Domain\Payments\Actions;

use App\Domain\Payments\Models\Payment;
use App\Support\Definitions\Status;
use App\Support\Interfaces\IAction;

class StorePayment implements IAction
{
    /**
     * @param array $params
     * @return bool
     */
    public static function execute(array $params): bool
    {
        $payment = new Payment();
        $payment->price = $params['price'];
        $payment->request_id = $params['requestId'];
        $payment->process_url = $params['processUrl'];
        $payment->payment_type = $params['payment_type'];
        $payment->status_id = Status::ACTIVE->value;
        $payment->order_id = $params['order_id'];
        return $payment->save();
    }
}
