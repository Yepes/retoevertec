<?php

namespace App\Domain\Payments\Actions;

use App\Domain\Payments\Models\Payment;
use App\Support\Definitions\Status;
use App\Support\Interfaces\IAction;

class UpdatePaymentStatus implements IAction
{
    /**
     * @param array $params
     * @return bool
     */
    public static function execute(array $params): bool
    {
        $status = Status::toArray();
        $res = 1;
        foreach ($status as $s)
        {
            if($s['description'] == $params['status_id'])
            {
                $res = $s['id'];
            }
        }
        return Payment::where('id', $params['id'])->update(['status_id' => $res ]);
    }
}
