<?php

namespace App\Domain\Products\Actions;

use App\Support\Interfaces\IAction;
use App\Support\Definitions\Status;
use App\Domain\Products\Models\Product;

class DisableProductsByCategory implements IAction
{
    public static function execute(array $params): bool
    {
        return Product::where('category_id', $params['category_id'])->update(
            ['status_id' => Status::INACTIVE->value]
        ) > 0;
    }
}