<?php

namespace App\Domain\Products\Actions;

use App\Support\Interfaces\IAction;
use App\Domain\Products\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StoreProduct implements IAction
{
    public static function execute(array $params): bool
    {
        $response = false;

        try {
            $nameImage =str_replace(' ', '_', $params['description']);
            $product = new Product();
            $product->description = $params['description'];
            $product->about = $params['about'];
            $product->slug = Str::slug($params['description'], '-', 'es');
            $product->image = Storage::disk('public')->putFileAs(
                'products_images',
                $params['image'],
                $nameImage . substr(now(),0,10) . ".png");
            $product->price = $params['price'];
            $product->stock = $params['stock'];
            $product->category_id = $params['category_id'];
            $product->status_id = $params['status_id'];
            $response = $product->save();

        } catch (\Exception $e) {
            logger()->error($e->getMessage());
        }

        return $response;
    }
}
