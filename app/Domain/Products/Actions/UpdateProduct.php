<?php

namespace App\Domain\Products\Actions;

use App\Support\Interfaces\IAction;
use App\Domain\Products\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UpdateProduct implements IAction
{
    /**
     * @param array<string, array|Product> $params
     * @return bool
     */
    public static function execute(array $params): bool
    {
        $response = false;

        try {
            $fields = $params['fields'];
            $product = $params['product'];
            $nameImage =str_replace(' ', '_', $fields['description']);
            $fields['slug'] = Str::slug($fields['description'], '-', 'es');

            if (array_key_exists('image', $fields) && $fields['image'] !== null) {
                Storage::disk('public')->delete($product->image);
                $fields['image'] = Storage::disk('public')->putFileAs(
                    'products_images',
                    $fields['image'],
                    $nameImage . substr(now(),0,10) . ".png");
            } else {
                $fields['image'] = $product->image;
            }

            $response = $product->update($fields);
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
        }

        return $response;
    }
}
