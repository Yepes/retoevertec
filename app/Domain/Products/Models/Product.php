<?php

namespace App\Domain\Products\Models;

use App\Support\Definitions\Status;
use App\Domain\Categories\Models\Category;
use Database\Factories\ProductFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * @property string $description
 * @property string $about
 * @property string $slug
 * @property string $image
 * @property float $price
 * @property int $stock
 * @property Status|null $status_id
 * @property int $category_id
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'about',
        'image',
        'slug',
        'price',
        'stock',
        'status_id',
        'category_id'
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            get: fn(int $value) => match ($value) {
                Status::ACTIVE->value => Status::ACTIVE,
                Status::INACTIVE->value => Status::INACTIVE,
                default => null
            },
            set: static function ($value) {
                return $value ? Status::ACTIVE->value : Status::INACTIVE->value;
            }
        );
    }

    protected static function newFactory(): Factory
    {
        return ProductFactory::new();
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
