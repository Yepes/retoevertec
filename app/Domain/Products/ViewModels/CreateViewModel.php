<?php

namespace App\Domain\Products\ViewModels;

use App\Support\Definitions\Status;
use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use App\Support\ViewModels\ViewModel;

class CreateViewModel extends ViewModel
{
    public function __construct()
    {
        parent::__construct(new Product());
    }

    public function toArray(): array
    {
        return [
            'categories' => Category::where('status_id', Status::ACTIVE->value)->get()
        ];
    }
}