<?php

namespace App\Domain\Products\ViewModels;

use App\Support\Definitions\Status;
use App\Domain\Categories\Models\Category;
use App\Support\ViewModels\ViewModel;

class EditViewModel extends ViewModel
{
     public function toArray(): array
    {
        return [
            'product' => $this->model(),
            'categories' => Category::where('status_id', Status::ACTIVE->value)->get()
        ];
    }
}