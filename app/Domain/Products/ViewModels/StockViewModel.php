<?php

namespace App\Domain\Products\ViewModels;


use App\Support\ViewModels\ViewModel;

class StockViewModel extends ViewModel
{
     public function toArray(): array
    {
        return [
            'product' => $this->model()
        ];
    }
}