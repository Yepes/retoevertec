<?php

namespace App\Domain\Users\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Support\Definitions\Rol as RolDefinition;

class Rol extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * @throws \Exception
     */
    public function value(): RolDefinition
    {
        return match ($this->id) {
            RolDefinition::CUSTOMER->value => RolDefinition::CUSTOMER,
            RolDefinition::ADMINISTRATOR->value => RolDefinition::ADMINISTRATOR,
            default => throw new \Exception('Rol incorrecto!'),
        };
    }
}