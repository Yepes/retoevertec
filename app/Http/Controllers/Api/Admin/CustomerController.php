<?php

namespace App\Http\Controllers\Api\Admin;

use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Customer\ToogleStatusRequest;
use App\Http\Resources\Api\StandardResource;
use App\Support\Definitions\Rol;
use App\Support\Definitions\Status;
use App\Support\Exceptions\UnsupportedStatus;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filtered = $request->has('filter');
        $filter = $request->get('filter');

        $customersList = User::whereHas('rol', static function ($rolQuery) {
            $rolQuery->where('id', Rol::CUSTOMER->value);
        })->when($filtered && $filter, static function ($query) use ($filter) {
            $query->where('name', 'like', '%' . $filter . '%')
                ->orWhere('lastname', 'like', '%' . $filter . '%')
                ->orWhere('email', 'like', '%' . $filter . '%');
        })->latest('id')->paginate(5);

        return response()->json(new StandardResource($customersList));
    }

    public function toggleStatus(ToogleStatusRequest $request): JsonResponse
    {
        $params = $request->validated();

        try {
            $user = User::find($params['id']);

            $newStatus = match ($user->status_id) {
                Status::ACTIVE->value => Status::INACTIVE->value,
                Status::INACTIVE->value => Status::ACTIVE->value,
                Status::PENDING->value => Status::ACTIVE->value,
                default => throw new UnsupportedStatus(__('customers.error_status_update'))
            };

            $user->status_id = $newStatus;
            $user->save();

            $responseData = __('customers.success_update');
        }catch (UnsupportedStatus $e) {
            $responseData = $e->getMessage();
            Log::error($e->getMessage(), ['context' => 'Updating customer status', 'value' => $user->status_id]);
        }

        return response()->json(new StandardResource([$responseData]));
    }
}