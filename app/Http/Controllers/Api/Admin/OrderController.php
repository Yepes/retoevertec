<?php

namespace App\Http\Controllers\Api\Admin;

use App\Domain\Orders\Models\Order;
use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\StandardResource;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        return response()->json(
            new StandardResource(
                Order::select(
                    'orders.id',
                    'orders.bill',
                    'orders.status_id',
                    'orders.created_at',
                    'orders.user_id',
                    'orders.value',
                    'users.name as user')
                    ->where('user_id', $request->id)
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->latest('id')->paginate(5)
            )
        );
    }
}
