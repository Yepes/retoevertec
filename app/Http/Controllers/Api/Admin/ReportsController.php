<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Dashboard\ReportsRequest;
use App\Http\Resources\Api\StandardResource;
use App\Support\Definitions\Status;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function getReports(ReportsRequest $request): JsonResponse
    {

        $startDate = date('Y-m-d 00:00:00', strtotime($request->validated('start_date')));
        $endDate = date('Y-m-d 00:00:00', strtotime($request->validated('end_date')));

        return response()->json(
            new StandardResource([
                'more_sale_products' => $this->moreSaleProducts($startDate, $endDate),
                'more_sale_categories' => $this->moreSalecategories($startDate, $endDate),
                'more_added_products' => $this->moreAddedProducts($startDate, $endDate),
                'best_customers' => $this->bestCustomers($startDate, $endDate),
                'orders_status' => $this->orderStatus($startDate, $endDate),
                'payments_status' => $this->paymentStatus($startDate, $endDate)
            ])
        );
    }

    private function moreSaleProducts(string $startDate, string $endDate): array
    {
        //Productos mas vendidos
        return DB::table('products')
            ->selectRaw('products.description as label, sum(orders_products.quantity) as cantidad')
            ->join('orders_products', 'products.id', '=', 'orders_products.product_id')
            ->join('orders', 'orders_products.order_id', '=', 'orders.id')
            ->where('orders.status_id', '=', Status::APPROVED->value)
            ->whereBetween('orders.created_at', [$startDate, $endDate])
            ->groupBy('products.description')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }
    private function moreSaleCategories(string $startDate, string $endDate): array
    {
        //Categorias mas pedidos
        return DB::table('categories')
            ->selectRaw('categories.description as label, sum(products.id) as cantidad')
            ->join('products', 'categories.id', '=', 'products.category_id')
            ->join('orders_products', 'products.id', '=', 'orders_products.product_id')
            ->join('orders', 'orders_products.order_id', '=', 'orders.id')
            ->where('orders.status_id', '=', Status::APPROVED->value)
            ->whereBetween('orders.created_at', [$startDate, $endDate])
            ->groupBy('categories.description')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }

    private function moreAddedProducts(string $startDate, string $endDate): array
    {
        //Productos mas pedidos
        return DB::table('products')
            ->selectRaw('products.description as label, sum(orders_products.quantity) as cantidad')
            ->join('orders_products', 'products.id', '=', 'orders_products.product_id')
            ->join('orders', 'orders_products.order_id', '=', 'orders.id')
            ->whereBetween('orders.created_at', [$startDate, $endDate])
            ->groupBy('products.description')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }

    private function bestCustomers(string $startDate, string $endDate): array
    {
        //Mejores clientes
        return DB::table('orders')
            ->selectRaw('count(orders.id) as cantidad, email as label')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->where('orders.status_id', '=', Status::APPROVED->value)
            ->whereBetween('orders.created_at', [$startDate, $endDate])
            ->groupBy('orders.user_id')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }

    private function orderStatus(string $startDate, string $endDate): array
    {
        //Pedidos por estado
        return DB::table('orders')
            ->selectRaw('count(orders.id) as cantidad, status.description as label')
            ->join('status', 'orders.status_id', '=', 'status.id')
            ->whereBetween('orders.created_at', [$startDate, $endDate])
            ->groupBy('orders.status_id')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }
    private function paymentStatus(string $startDate, string $endDate): array
    {
        //Pagos por estado
        return DB::table('payments')
            ->selectRaw('count(payments.id) as cantidad, status.description as label')
            ->join('status', 'payments.status_id', '=', 'status.id')
            ->whereBetween('payments.created_at', [$startDate, $endDate])
            ->groupBy('payments.status_id')
            ->orderByRaw('cantidad desc')
            ->limit(10)
            ->get()->toArray();
    }

}
