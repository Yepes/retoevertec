<?php

namespace App\Http\Controllers\Web\Admin;

use App\Domain\Categories\Models\Category;
use App\Support\Definitions\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Category\CreateRequest;
use App\Http\Requests\Web\Category\UpdateRequest;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class CategoryController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('Admin/Category/Index');
    }

    public function create(): Response
    {
        return Inertia::render('Admin/Category/Create');
    }

    public function store(CreateRequest $request): RedirectResponse
    {
        $params = $request->validated();
        
        $params['status_id'] == "" ? $params['status_id'] = Status::INACTIVE->value : "" ;
        if (Category::create($params)) {
            session()->flash('success', __('categories.success_create'));
        } else {
            session()->flash('error', __('categories.error_create'));
        }

        return redirect()->route('categories.index');
    }

    public function show(Category $category): Response
    {
        return Inertia::render('Admin/Category/Edit', ['category' => $category]);
    }

    public function update(Category $category, UpdateRequest $request): RedirectResponse
    {
        $params = $request->validated();

        if ($category->update($params)) {
            session()->flash('success', __('categories.success_update'));
        } else {
            session()->flash('error', __('categories.error_update'));
        }

        return redirect()->route('categories.index');
    }
}