<?php

namespace App\Http\Controllers\Web\Admin;

use App\Domain\Products\Actions\StoreProduct;
use App\Domain\Products\Actions\UpdateProduct;
use App\Support\Definitions\Status;
use App\Domain\Products\Models\Product;
use App\Domain\Products\ViewModels\CreateViewModel;
use App\Domain\Products\ViewModels\StockViewModel;
use App\Domain\Products\ViewModels\EditViewModel;
use App\Domain\Products\ViewModels\ListViewModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Product\CreateRequest;
use App\Http\Requests\Web\Product\UpdateRequest;
use App\Http\Requests\Web\Product\StockRequest;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class ProductController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('Admin/Product/Index', new ListViewModel());
    }

    public function create(): Response
    {
        return Inertia::render('Admin/Product/Create', new CreateViewModel());
    }

    public function store(CreateRequest $request): RedirectResponse
    {
        $params = $request->validated();

        $params['status_id'] == 0 ? $params['status_id'] = Status::INACTIVE->value : "" ;

        if (StoreProduct::execute($params)) {
            session()->flash('success', __('products.success_create'));
        } else {
            session()->flash('error', __('products.error_create'));
        }

        return redirect()->route('products.index');
    }

    public function show(Product $product): Response
    {
        return Inertia::render('Admin/Product/Edit', new EditViewModel($product));
    }

    public function update(Product $product, UpdateRequest $request): RedirectResponse
    {
        if (UpdateProduct::execute(['fields' => $request->validated(), 'product' => $product])) {
            session()->flash('success', __('products.success_update'));
        } else {
            session()->flash('error', __('products.error_update'));
        }

        return redirect()->route('products.index');
    }

    public function showStock(Product $product): Response
    {
        return Inertia::render('Admin/Product/Stock', new StockViewModel($product));
    }

    public function stock(Product $product, StockRequest $request): RedirectResponse
    {
        $params = $request->validated();

        $params['stock'] += $product->stock;

        if ($product->update($params)) {
            session()->flash('success', __('products.success_update'));
        } else {
            session()->flash('error', __('products.error_update'));
        }

        return redirect()->route('products.index');
    }
}
