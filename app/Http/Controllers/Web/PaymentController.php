<?php

namespace App\Http\Controllers\Web;

use App\Domain\Orders\Actions\ValidateOrderStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\Payments\Actions\StorePayment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Payment\CreatePaymentRequest;
use App\Support\Services\PaymentFactory;
use Exception;
use Illuminate\Contracts\Foundation\Application as ApplicationContracts;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;

class PaymentController extends Controller
{
    /**
     * @throws Exception
     */
    public function create(
        CreatePaymentRequest $request,
        PaymentFactory $paymentFactory
    ): ApplicationContracts|RedirectResponse|Application {
        if (!ValidateOrderStatus::execute($request->validated())) {
            return redirect()->route('orders.index');
        }

        $price = Order::find($request->validated('order_id'));
        $processor = $paymentFactory->initializePayment($request->get('payment_type'));
        $data = $processor->setUpPayment($request)->pay();
        $data['order_id'] = $request->validated('order_id');
        $data['payment_type'] = $request->validated('payment_type');
        $data['price'] = $price->value;

        StorePayment::execute($data);

        return redirect($data['processUrl']);
    }
}
