<?php

namespace App\Jobs;

use App\Domain\Orders\Actions\UpdateOrderStatus;
use App\Domain\Payments\Actions\UpdatePaymentStatus;
use App\Support\Definitions\Status;
use App\Support\Services\PaymentFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessPendingPayments implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Execute the job.
     */
    public function handle(PaymentFactory $paymentFactory): void
    {
        DB::transaction(static function () use ($paymentFactory) {
            $paymentInfo = DB::table('orders')
                ->select([
                    'orders.id as order_id',
                    'payments.id as payment_id',
                    'payments.status_id as payment_status_id',
                    'payments.request_id',
                    'payments.payment_type'
                ])
                ->join('payments', function (JoinClause $join) {
                    $join->on('orders.id', '=', 'payments.order_id')
                        ->whereIn('payments.status_id', [
                            Status::ACTIVE->value,
                            Status::PENDING->value,
                            Status::APPROVED_PARTIAL->value,
                            Status::PARTIAL_EXPIRED->value
                        ]);
                })
                ->where('orders.status_id', Status::ACTIVE->value)
                ->get();

            foreach ($paymentInfo as $payment) {
                Log::debug("PAYMENT: " . $payment->payment_id . "----se encuentra en estado: " . $payment->payment_status_id);

                $processor = $paymentFactory->initializePayment($payment->payment_type);
                $status = $processor->getPaymentStatus((string)$payment->request_id);
                UpdatePaymentStatus::execute(['id' => $payment->id, 'status_id' => $status]);

                if ($status === Status::APPROVED->value) {
                    UpdateOrderStatus::execute(['id' => $payment->id, 'status_id' => Status::APPROVED->value]);
                }
            }
        });
    }
}
