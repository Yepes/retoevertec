<?php

namespace App\Support\Definitions;

enum Rol: int
{
    case ADMINISTRATOR = 1;
    case CUSTOMER = 2;

    public static function toArray(): array
    {
        $cases = self::cases();
        $array = [];

        foreach ($cases as $case) {
            $array[] = ['id' => $case->value, 'description' => ucfirst($case->name)];
        }

        return $array;
    }
}