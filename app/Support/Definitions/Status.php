<?php

namespace App\Support\Definitions;

enum Status: int
{
    case ACTIVE = 1;
    case INACTIVE = 2;
    case PENDING = 3;
    case APPROVED = 4;
    case REJECTED = 5;
    case FAILED = 6;
    case APPROVED_PARTIAL = 7;
    case PARTIAL_EXPIRED = 8;

    public static function toArray(): array
    {
        $cases = self::cases();
        $array = [];

        foreach ($cases as $case) {
            $array[] = ['id' => $case->value, 'description' => ucfirst($case->name)];
        }

        return $array;
    }

    public static function toJson(): array
    {
        $cases = self::cases();
        $array = [];

        foreach ($cases as $case) {
            $array[ucfirst($case->name)] = $case->value;
        }

        return $array;
    }
}