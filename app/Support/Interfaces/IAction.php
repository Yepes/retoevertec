<?php

namespace App\Support\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface IAction
{
    public static function execute(array $params): bool|int|Model;
}