<?php

namespace App\Support\Interfaces;

interface IReadOnlyRepository
{
    // Define tus métodos aquí
    public function getOne($id);
    public function getAll();
}
