<?php

namespace App\Support\Interfaces;

interface IWriteModifyRepository
{
    // Define tus métodos aquí
    public function create(array $data);
    //public function update(Object $object, Object $request, Object $dao);
    public function delete($id);
}
