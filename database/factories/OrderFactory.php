<?php

namespace Database\Factories;

use App\Domain\Orders\Models\Order;
use App\Support\Definitions\Status;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Order>
 */
class OrderFactory extends Factory
{
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'date' =>  date('Y-m-d'),
            'bill' =>  Str::random(3) . time(),
            'status_id' => Status::ACTIVE->value,
            'value' => fake()->numberBetween(1, 500000),
            'user_id' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}