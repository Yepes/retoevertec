<?php

namespace Database\Factories;

use App\Domain\Categories\Models\Category;
use App\Domain\Products\Models\Product;
use App\Support\Definitions\Status;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     *
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        if (env('APP_ENV') === 'testing') {
            Storage::fake('public');
        }

        $description = fake()->name();

        return [
            'description' => $description,
            'about' => fake()->sentence(10),
            'slug' => str::slug($description,'-','es'),
            'image' => Storage::disk('public')->put(
                'products_images',
                UploadedFile::fake()->image('image.png', 1200, 1200)
            ),
            'price' => fake()->randomNumber(5),
            'stock' => fake()->randomNumber(2),
            'status_id' => Status::ACTIVE->value,
            'category_id' => fake()->randomElement(Category::all())['id']
        ];
    }
}
