<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars_products', function (Blueprint $table) {
            $table->foreignId('car_id')->constrained();
            $table->foreignId('product_id')->constrained();
            $table->decimal('price');
            $table->integer('quantity');
            $table->timestamps();
            $table->primary(['car_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars_products');
    }
};
