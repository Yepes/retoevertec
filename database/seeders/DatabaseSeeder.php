<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Domain\Categories\Models\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $this->call([
            RolSeeder::class,
            StatusSeeder::class
        ]);

        Category::factory()->create(['description' => 'General']);

        if (env('APP_ENV') !== 'production') {
            $this->call([
                UserSeeder::class,
                CategorySeeder::class,
                ProductSeeder::class
            ]);
        }

    }
}
