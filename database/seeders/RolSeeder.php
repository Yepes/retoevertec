<?php

namespace Database\Seeders;

use App\Support\Definitions\Rol;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('rols')->insert(Rol::toArray());
    }
}
