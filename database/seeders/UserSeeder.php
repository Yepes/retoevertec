<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Domain\Users\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Create 500 users
        User::factory()->count(500)->create();
    }
}
