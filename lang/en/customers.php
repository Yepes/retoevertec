<?php

return [
    'title' => 'Customers',
    'list' => 'Customers list',
    'edit' => 'Edit customer',
    'success_update' => 'User updated successfully!',
    'error_update' => 'Can not update user',
    'error_status_update' => 'Could not update customer',
];
