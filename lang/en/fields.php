<?php

return [
    'name' => 'Name',
    'names' => 'Name',
    'lastname' => 'Lastname',
    'description' => 'Description',
    'category' => 'Category',
    'price' => 'Price',
    'stock' => 'Stock',
    'current_stock' => 'Current stock',
    'add_stock' => 'Add stock',
    'add_stock_warning' => 'This value may change when saving changes',
    'status' => 'Status',
    'image' => 'Image',
    'phone' => 'Phone',
    'email' => 'Email',
    'bill' => 'Bill',
    'created_at' => 'Create at'
];
