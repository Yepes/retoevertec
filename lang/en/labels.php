<?php

return [
    'search' => 'Search',
    'create' => 'Create',
    'actions' => 'Actions',
    'back' => 'Back',
    'save' => 'Save',
    'add_cart' => 'Add to cart',
    'added_cart' => 'Added to cart!',
    'subtotal' => 'Subtotal',
    'total' => 'Total',
    'yes' => 'Yes',
    'no' => 'No',
    'stock_error' => 'Amount not in stock',
    'stock' => 'Stock',
    'import' => 'Import',
    'export' => 'Export',
    'download' => 'Download',
    'file_upload' => 'File upload'
];
