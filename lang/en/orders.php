<?php

return [
    'title' => 'Order',
    'plural_title' => 'Orders',
    'time_limit_message' => 'Once your order is placed, you must pay within a maximum period of 60 minutes, otherwise, the order will be automatically canceled',
    'status' => [
        '1' => 'Active',
        '2' => 'Canceled',
        '4' => 'Approved'
    ],
    'list' => 'Orders list',
    'no_records' => 'There is not orders'
];