<?php

return [
    'title' => 'Products',
    'list' => 'Products list',
    'create' => 'Create product',
    'edit' => 'Edit product',
    'add_stock_title' => 'Add stock',
    'success_create' => 'Product created successfully!',
    'success_update' => 'Product updated successfully!',
    'error_create' => 'Can not create product',
    'error_update' => 'Can not update product',
    'error_status_update' => 'Could not update product',
    'unit_price' => 'Unit price',
    'import' => 'Import products',
    'import_alert' => [
        'The category will be created if it does not exists.',
        'The status column must have the values active or inactive.',
        'You will receive an email when the import process is completed.',
        'If you want to create a new product leave the id column empty.'
    ],
    'import_format_file' => 'In order to import the products you must fill out and upload the following form',
    'import_started' => 'Import process has been started.',
    'import_successfully' => 'Product import process completed successfully',
    'import_error' => 'An error occurred while importing products.',
    'export_started' => "The export process has been started, you will receive an email with the download link when it's completed",
    'export_success' => 'Products export process completed successfully'
];
