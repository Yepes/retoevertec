<?php

return [
    'title' => 'Categorías',
    'list' => 'Lista de categorías',
    'create' => 'Crear categoría',
    'edit' => 'Editar categoría',
    'success_create' => '¡Categoría creada exitosamente!',
    'success_update' => '¡Categoría actualizada exitosamente!',
    'error_create' => 'No se pudo crear la categoría',
    'error_update' => 'No se pudo actualizar la categoría',
    'error_status_update' => 'No se pudo actualizar la categoría',
    'no_records' => 'No se encontraron categorías'
];
