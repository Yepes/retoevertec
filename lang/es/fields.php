<?php

return [
    'name' => 'Nombre',
    'names' => 'Nombres',
    'lastname' => 'Apellidos',
    'description' => 'Descripción',
    'category' => 'Categoría',
    'price' => 'Precio',
    'stock' => 'Cantidad',
    'current_stock' => 'Cantidad actual',
    'add_stock' => 'Agregar cantidad',
    'add_stock_warning' => 'Este valor puede variar al guardar los cambios',
    'status' => 'Estado',
    'image' => 'Imagen',
    'phone' => 'Teléfono',
    'email' => 'Correo electrónico'
];
