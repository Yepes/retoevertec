<?php

return [
    'search' => 'Buscar',
    'create' => 'Crear',
    'actions' => 'Acciones',
    'back' => 'Atrás',
    'save' => 'Guardar',
    'add_cart' => 'Agregar al carrito',
    'added_cart' => 'Agregado al carrito!',
    'subtotal' => 'Subtotal',
    'total' => 'Total',
    'yes' => 'Si',
    'no' => 'No',
    'stock_error' => 'La cantidad seleccionada supera las existencias',
    'stock' => 'Existencias',
    'import' => 'Importar',
    'export' => 'Exportar',
    'download' => 'Descargar',
    'file_upload' => 'Cargar archivo'
];
