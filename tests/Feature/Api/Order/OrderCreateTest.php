<?php

namespace Api\Order;

use App\Domain\Users\Models\User;
use App\Support\Definitions\Status;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderCreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest_cant_create_order(): void
    {
        $this->post(route('api.orders.store'))
            ->assertFound()->assertRedirect(route('home'));
    }

    public function test_customer_can_create_order(): void
    {
        $user = User::factory()->create();
        $resp = $this->actingAs($user)->post(
            route('api.orders.store'),
            [
                'products' => [
                    1 => ['id' => 1, 'amount' => 5],
                    2 => ['id' => 2, 'amount' => 2]
                ]
            ]
        );

        $order = $user->orders->first();

        $resp->assertSessionDoesntHaveErrors()
            ->assertOk()
            ->assertJson([
                'data' => [
                    'route' => route('orders.show', $order->id),
                    'clear_cart' => true,
                    'set_max_amounts' => false
                ]
            ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => $user->id,
            'status_id' => Status::ACTIVE->value
        ]);

        $this->assertDatabaseHas('orders_products', ['product_id' => 1, 'order_id' => $order->id, 'quantity' => 5])
            ->assertDatabaseHas('orders_products', ['product_id' => 2, 'order_id' => $order->id, 'quantity' => 2]);
    }

    public function test_products_out_of_stock(): void
    {
        $user = User::factory()->create();
        $resp = $this->actingAs($user)->post(
            route('api.orders.store'),
            [
                'products' => [
                    1 => ['id' => 1, 'amount' => 100],
                    2 => ['id' => 2, 'amount' => 100]
                ]
            ]
        );

        $resp->assertSessionDoesntHaveErrors()
            ->assertOk()
            ->assertJson([
                'data' => [
                    'route' => false,
                    'clear_cart' => false,
                    'set_max_amounts' => true
                ]
            ]);
    }
}
