<?php

namespace Api\Product;

use App\Domain\Products\Models\Product;
use App\Domain\Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductGetForCartTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    public function test_access_list(): void
    {
        $p1 = Product::find(1);
        $p2 = Product::find(2);

        $this->actingAs($this->user)
            ->post(route('api.getCartProducts'), ['ids' => [$p1->id, $p2->id]])
            ->assertOk()
            ->assertSimilarJson([
                'data' => [
                    [
                        'id' => $p1->id,
                        'description' => $p1->description,
                        'slug' => $p1->slug,
                        'image' => $p1->image,
                        'price' => $p1->price,
                        'stock' => $p1->stock
                    ],
                    [
                        'id' => $p2->id,
                        'description' => $p2->description,
                        'slug' => $p2->slug,
                        'image' => $p2->image,
                        'price' => $p2->price,
                        'stock' => $p2->stock
                    ]
                ]
            ]);
    }
}