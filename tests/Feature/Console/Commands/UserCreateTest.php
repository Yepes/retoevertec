<?php

namespace Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserCreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user(): void
    {
        $this->artisan('app:create-admin')
            ->expectsQuestion('Correo electronico?', 'admin@admin.com')
            ->expectsQuestion('Clave?', '12345678')
            ->expectsOutputToContain('Usuario creado!')
            ->doesntExpectOutputToContain('Error al crear el usuario')
            ->assertExitCode(0);
    }

    public function test_create_user_error(): void
    {
        $this->artisan('app:create-admin')
            ->expectsQuestion('Correo electronico?', 'admin@admin.com')
            ->expectsQuestion('Clave?', '12345678')
            ->expectsOutputToContain('Usuario creado!')
            ->assertExitCode(0);

        $this->artisan('app:create-admin')
            ->expectsQuestion('Correo electronico?', 'admin@admin.com')
            ->expectsQuestion('Clave?', '12345678')
            ->expectsOutputToContain('Error al crear usuario')
            ->assertExitCode(0);
    }
}
