<?php

namespace Tests\Feature\Jobs;

use App\Domain\Users\Models\User;
use App\Jobs\ProductExportJob;
use App\Mail\ExportMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ProductExportJobTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_export_file(): void
    {
        Storage::fake("public");
        Mail::fake();
        (new ProductExportJob(User::factory()->admin()->create()))->handle();
        Storage::disk('public')->assertExists('exports/products_inventory.csv');
        Mail::assertSent(ExportMail::class);
    }
}
