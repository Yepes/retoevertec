<?php

namespace Tests\Feature\Web\Admin\Category;

use App\Domain\Users\Models\User;
use App\Support\Definitions\Status;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryCreateTest extends TestCase
{
    use RefreshDatabase;

    private User $adminUser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->adminUser = User::factory()->admin()->create();
    }

    public function test_category_cant_access_form(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('categories.create'));

        $response->assertFound()->assertRedirect(route('home'));
    }

    public function test_admin_can_access_form(): void
    {
        $response = $this->actingAs($this->adminUser)->get(route('categories.create'));
        $response->assertOk();
    }

    public function test_can_create_category(): void
    {
        $newCategory = [
            'description' => fake()->name(),
            'status_id' => Status::ACTIVE->value
        ];
        $response = $this->actingAs($this->adminUser)->post(route('categories.store'), $newCategory);
        $response->assertSessionHas('success');
        $response->assertRedirect(route('categories.index'));
    }

}
