<?php

namespace Tests\Feature\Web\Admin\Product;

use App\Domain\Categories\Models\Category;
use App\Domain\Users\Models\User;
use App\Support\Definitions\Status;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ProductUpdateTest extends TestCase
{
    use RefreshDatabase;

    private User $adminUser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->adminUser = User::factory()->admin()->create();
    }

    public function test_customer_cant_access_form(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('products.show', 1));

        $response->assertFound()->assertRedirect(route('home'));
    }

    public function test_admin_can_access_form(): void
    {
        $response = $this->actingAs($this->adminUser)->get(route('products.show', 1));
        $response->assertOk();
    }

    public function test_update_product_without_image(): void
    {
        $newData = [
            'id' => 1,
            'description' => fake()->name(),
            'about' => fake()->sentence(10),
            'status_id' => Status::INACTIVE->value,
            'price' => fake()->randomNumber(4),
            'category_id' => Category::first()->id,
            'image' => null
        ];
        $response = $this->actingAs($this->adminUser)->put(route('products.update', 1), $newData);
        $response->assertSessionHas('success');
        $response->assertRedirect(route('products.index'));
    }

    public function test_update_product_with_image(): void
    {
        $newData = [
            'id' => 1,
            'description' => fake()->name(),
            'about' => fake()->sentence(10),
            'status_id' => Status::INACTIVE->value,
            'price' => fake()->randomNumber(4),
            'category_id' => Category::first()->id,
            'image' => UploadedFile::fake()->image('product_image.png', 1200, 1200)
        ];
        $response = $this->actingAs($this->adminUser)->put(route('products.update', 1), $newData);
        $response->assertSessionHas('success');
        $response->assertRedirect(route('products.index'));
    }
}
